#include <iostream>
#include <string>
#include "UnparseFormat.h"

using namespace std;
using namespace SageInterface;
using namespace SageBuilder;

class FunctionConstructor {
	SgExprListExp *params;
	SgVarRefExp *var_ref;
public:
	void add_param(SgExpression *e);		
	void set_var_ref(SgVarRefExp *ref) {var_ref = deepCopy(ref);}
	void insert_f(SgExpression *where, int dimensions);
	FunctionConstructor();
};

void FunctionConstructor::add_param(SgExpression *e)
{
	params->append_expression(deepCopy(e));
}

void FunctionConstructor::insert_f(SgExpression *where, int dimensions) 
{
	params->prepend_expression(buildStringVal("double"));
	params->prepend_expression(var_ref);

	SgName f_name = SgName(string("DVMad") + string(to_string((long long unsigned int)dimensions)));
	// clog << "scope is " << (getEnclosingStatement(where)->get_scope() == NULL ? "NULL" : "NOT NULL") << endl;
	SgExpression* f_call = buildFunctionCallExp(f_name, buildDoubleType(), params, getEnclosingStatement(where)->get_scope());

	clog << "call: " << f_call->unparseToString() << endl;
	replaceExpression(where, f_call);
	clog << "inserted function call" << endl;
}

FunctionConstructor::FunctionConstructor()
{
	params = buildExprListExp();
}

typedef int InheritedAttribute;

class GlobalTraversal: public AstSimpleProcessing {
public:
	virtual void visit(SgNode* n);
};

class LocalTraversal: public AstTopDownProcessing<InheritedAttribute> {
	int array_dimensions;
	FunctionConstructor f;
public:
	LocalTraversal(): array_dimensions(0), f() {}
	int get_dimensions() {return array_dimensions;}
	FunctionConstructor &get_constructor() {return f;}
	virtual InheritedAttribute evaluateInheritedAttribute(SgNode* n, InheritedAttribute parent_attr);
};

InheritedAttribute LocalTraversal::evaluateInheritedAttribute(SgNode* n, InheritedAttribute parent_attr)
{
	clog << "inherited attribute at " << n->sage_class_name() << endl;
	if (isSgPntrArrRefExp(n) && (!array_dimensions || (!parent_attr && (n == n->get_parent()->get_traversalSuccessorByIndex(0))))) {
		clog << "setting 0 as inh_attr" << endl;
		array_dimensions++;
		return 0;
	} else {
		clog << "setting " << parent_attr + 1 << " as inh_attr" << endl;
		if (parent_attr == 0) {
			SgVarRefExp *var = isSgVarRefExp(n);
			if (var) {
				clog << "recording var name" << endl;
				f.set_var_ref(var);
			} else {
				clog << "adding a parameter" << endl;
				SgExpression *exp = isSgExpression(n);
				if (exp)
					f.add_param(exp);
				else
					throw string("non-expression as index?");
			}
		}
		return parent_attr + 1;
	}
}

void GlobalTraversal::visit(SgNode* n) 
{
	SgPntrArrRefExp* ptr_assignment = isSgPntrArrRefExp(n);
	if (ptr_assignment != NULL) {
		LocalTraversal local_traversal;
		local_traversal.traverse(n, 0);
		clog << "dimensions: " << local_traversal.get_dimensions() << endl;
		if (local_traversal.get_dimensions() > 1) {
			local_traversal.get_constructor().insert_f(ptr_assignment, local_traversal.get_dimensions());
			throw string();
		}
	}
}

int main(int argc, char * argv[])
{
	SgProject* project = frontend(argc,argv);

	for (;;) {
		try {
			GlobalTraversal exampleTraversal;
			exampleTraversal.traverseInputFiles(project, preorder);
			break;
		} catch (string s) {
		}
	}

	UnparseFormatHelp *f = new UnparseFormatCustom();
	project->unparse(f);
	return 0; 
}

