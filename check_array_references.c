// Example ROSE Translator: used within ROSE/tutorial

#include <iostream>
#include <string>
#include "UnparseFormat.h"

using namespace std;
using namespace SageInterface;
using namespace SageBuilder;

class VisitorTraversal: public AstSimpleProcessing {
	SgScopeStatement *global_scope;
	bool inserted_dvm_3_decl;
	void handle_array_ref(SgStatement *ref, SgVarRefExp *var, SgExpression *shift);
	void declare_ok_f(SgScopeStatement *where);
public:
	virtual void visit(SgNode* n);
	VisitorTraversal(SgProject* project_ptr); 
};

VisitorTraversal::VisitorTraversal(SgProject* project_ptr) 
{
	inserted_dvm_3_decl = false;
	global_scope = getFirstGlobalScope(project_ptr);
}

void VisitorTraversal::declare_ok_f(SgScopeStatement *where)
{
	SgName fun_dvm_t("dvm_is_ok");

	SgFunctionParameterTypeList fun_dvm_t_par;
	fun_dvm_t_par.append_argument(buildPointerType(buildVoidType()));
	fun_dvm_t_par.append_argument(buildIntType());

	SgFunctionDeclaration *dvm_t_decl;
	dvm_t_decl = buildNondefiningFunctionDeclaration(fun_dvm_t,  buildVoidType(), buildFunctionParameterList(&fun_dvm_t_par)); 
	((dvm_t_decl->get_declarationModifier()).get_storageModifier()).setExtern();
	prependStatement(dvm_t_decl, where);
	clog << "inserted function declaration" << endl;
	inserted_dvm_3_decl = true;
}

void VisitorTraversal::handle_array_ref(SgStatement *ref, SgVarRefExp *var, SgExpression *shift)
{
	if (!inserted_dvm_3_decl) {
		declare_ok_f(getGlobalScope(ref));
	}

	SgExprListExp *fun_param_list = new SgExprListExp;
	fun_param_list->append_expression(var);
	fun_param_list->append_expression(shift);

	SgExprStatement* tmp = buildFunctionCallStmt(SgName("dvm_is_ok"), buildVoidType(), fun_param_list, getScope(ref));
	replaceStatement(ref, tmp);
	// SgExprStatement* dvm_t_call_stmt = buildFunctionCallExp(dvm_t_decl->search_for_symbol_from_symbol_table(), buildExprListExp(params));
	SgExprStatement* dvm_t_call_stmt = buildFunctionCallStmt(SgName("dvm_is_ok"), buildVoidType(), fun_param_list, getScope(ref));

	SgIfStmt *ins_if = buildIfStmt(dvm_t_call_stmt, ref, NULL);
	replaceStatement(tmp, ins_if);
	clog << "replaced statement" << endl;
	// removeStatement(getEnclosingNode<SgStatement>(ref));
}

void VisitorTraversal::visit(SgNode* n) 
{
	SgLocatedNode* ptr_assignment = isSgPntrArrRefExp(n);
	if (ptr_assignment != NULL) {
		SgVarRefExp *var = isSgVarRefExp(n->get_traversalSuccessorByIndex(0));
		if (!var)
			return;
		SgExpression *shift = isSgExpression(n->get_traversalSuccessorByIndex(1));
		if (!shift)
			return;
		SgStatement *statement = getEnclosingStatement(ptr_assignment);
		SgNode *p = n->get_parent();
		SgNode *pp = p->get_parent();
		if ((isSgCompoundAssignOp(p) || isSgAssignOp(p)) && isSgStatement(pp))
			handle_array_ref(statement, deepCopy(var), deepCopy(shift));
	}
}

int main(int argc, char * argv[])
{
	SgProject* project = frontend(argc,argv);

	VisitorTraversal exampleTraversal(project);
	exampleTraversal.traverseInputFiles(project, preorder);

	UnparseFormatHelp *f = new UnparseFormatCustom();
	project->unparse(f);
	return 0; 
}

