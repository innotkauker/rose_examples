extern void dvm_is_ok(void *,int );

int main()
{
  
#pragma dvm hello world
  do {
    
#pragma dvm hello world
    
#pragma dvmh bye
  }while (0);
  
#pragma dvm template abc
  do {
    int x = 0;
    int t[90UL];
    if (dvm_is_ok(t,1)) 
      t[1] += (t[7] = 5);
    if (dvm_is_ok(t,(6 * x) + 2)) 
      t[(6 * x) + 2] = 587;
  }while (0);
  return 0;
}
