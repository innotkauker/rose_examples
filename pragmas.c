#include <iostream>
#include <string>
#include <vector>
#include "UnparseFormat.h"

using namespace std;
using namespace SageInterface;
using namespace SageBuilder;
using namespace AstFromString;

class VisitorTraversal: public AstSimpleProcessing {
	vector<SgPragmaDeclaration *> pragmas;
public:
	virtual void visit(SgNode* n);
	vector<SgPragmaDeclaration *> &get_pragmas() {return pragmas;}
};

void VisitorTraversal::visit(SgNode* n) 
{
	SgPragmaDeclaration* pragma = isSgPragmaDeclaration(n);
	if (pragma) {
		pragmas.push_back(pragma);
	}
}

class PragmaHandler {
	bool inserted_dvm_1_decl;
	bool inserted_dvm_2_decl;

	void handle_dvm_1(SgPragmaDeclaration *pragma, SgVarRefExp *var, SgExpression *expr);
	void handle_dvm_2(SgPragmaDeclaration *pragma, SgVarRefExp *var, SgExpression *expr);
	void declare_functions(SgPragmaDeclaration *pragma);
	void recognize_dvm_1(SgPragmaDeclaration *pragma_decl);
	void recognize_dvm_2(SgPragmaDeclaration *pragma_decl);
	bool afs_match_undefined_identifier(string &id);
public:
	void handle_pragma(SgPragmaDeclaration *pragma_decl);
	PragmaHandler();
};

PragmaHandler::PragmaHandler()
{
	inserted_dvm_1_decl = false;
	inserted_dvm_2_decl = false;
}

void PragmaHandler::handle_dvm_2(SgPragmaDeclaration *pragma, SgVarRefExp *var, SgExpression *expr)
{
	// add variable definition
	// long <identifier1>;
	SgVariableDeclaration *var_declaration = buildVariableDeclaration(var->get_symbol()->get_name(), buildLongType());
	insertStatement(getEnclosingStatement(pragma), var_declaration);
	clog << "inserted variable declaration" << endl;
	// add function call
	// dvm_template(<identifier1>, <expression1>);
	if (!inserted_dvm_1_decl) {
		SgName fun_dvm_t("dvm_template");

		SgFunctionParameterTypeList fun_dvm_t_par;
		fun_dvm_t_par.append_argument(buildIntType());
		fun_dvm_t_par.append_argument(buildLongType());

		SgFunctionDeclaration *dvm_t_decl;
		dvm_t_decl = buildNondefiningFunctionDeclaration(fun_dvm_t,  buildVoidType(), buildFunctionParameterList(&fun_dvm_t_par)); 
		((dvm_t_decl->get_declarationModifier()).get_storageModifier()).setExtern(); // declare and mark as external
		prependStatement(dvm_t_decl, getGlobalScope(pragma));
		// insertStatement(pragma, dvm_t_decl);
		clog << "inserted function definition" << endl;
		inserted_dvm_1_decl = true;
	}

	SgExprListExp *fun_param_list = buildExprListExp();
	fun_param_list->append_expression(var);
	fun_param_list->append_expression(expr);

	// SgExprStatement* dvm_t_call_stmt = buildFunctionCallExp(dvm_t_decl->search_for_symbol_from_symbol_table(), buildExprListExp(params));
	SgExprStatement* dvm_t_call_stmt = buildFunctionCallStmt(SgName("dvm_template"), buildVoidType(), fun_param_list, pragma->get_scope());

	// dvm_t_call_stmt->set_firstNondefiningDeclaration(	dvm_t_call_stmt);

	// SgVarRefExp* block = buildVarRefExp(fun_dvm_t, SageInterface::getGlobalScope(pragma));
	insertStatement(pragma, dvm_t_call_stmt);
	clog << "inserted function call" << endl;
	removeStatement(pragma);
	clog << "removed pragma" << endl;
}

// this turned out to be unnecessary, but as i spent a lot of time writing, debugging and practising shamanism, it shall stay.
void PragmaHandler::declare_functions(SgPragmaDeclaration *pragma)
{
	SgName fun_dvm_start("dvm_start");
	SgName fun_dvm_seen("dvm_seen");
	SgName fun_dvm_end("dvm_end");
	
	SgFunctionParameterTypeList fun_dvm_start_par;
	fun_dvm_start_par.append_argument(buildIntType());

	SgFunctionParameterTypeList fun_dvm_seen_par;
	fun_dvm_seen_par.append_argument(buildIntType());
	fun_dvm_seen_par.append_argument(buildDoubleType());

	SgFunctionParameterTypeList fun_dvm_end_par;
	fun_dvm_end_par.append_argument(buildIntType());

	// start() declaration
	SgFunctionDeclaration *dvm_start_decl;
	dvm_start_decl = buildNondefiningFunctionDeclaration(fun_dvm_start,  buildVoidType(), buildFunctionParameterList(&fun_dvm_start_par)); 
	((dvm_start_decl->get_declarationModifier()).get_storageModifier()).setExtern();
	prependStatement(dvm_start_decl, getGlobalScope(pragma));

	// seen() declaration
	SgFunctionDeclaration *dvm_seen_decl;
	dvm_seen_decl = buildNondefiningFunctionDeclaration(fun_dvm_seen,  buildVoidType(), buildFunctionParameterList(&fun_dvm_seen_par)); 
	((dvm_seen_decl->get_declarationModifier()).get_storageModifier()).setExtern();
	prependStatement(dvm_seen_decl, getGlobalScope(pragma));

	// end() declaration
	SgFunctionDeclaration *dvm_end_decl;
	dvm_end_decl = buildNondefiningFunctionDeclaration(fun_dvm_end,  buildVoidType(), buildFunctionParameterList(&fun_dvm_end_par)); 
	((dvm_end_decl->get_declarationModifier()).get_storageModifier()).setExtern();
	prependStatement(dvm_end_decl, getGlobalScope(pragma));
	clog << "inserted definitions for 3 functions" << endl;
	inserted_dvm_2_decl = true;
}

void PragmaHandler::handle_dvm_1(SgPragmaDeclaration *pragma, SgVarRefExp *var, SgExpression *expr)
{
	// add function calls
	// dvm_start(<identifier1>);
	// dvm_seen(<identifier1>, <expression2>);
	// dvm_end(<identifier1>);

	if (!inserted_dvm_2_decl) {
		declare_functions(pragma);
	}

	SgExprListExp *fun_param_list = buildExprListExp();
	fun_param_list->append_expression(var);
	SgExprStatement* dvm_start_call_stmt = buildFunctionCallStmt(SgName("dvm_start"), buildVoidType(), fun_param_list, pragma->get_scope());
	insertStatement(pragma, dvm_start_call_stmt);

	fun_param_list = buildExprListExp();
	fun_param_list->append_expression(var);
	fun_param_list->append_expression(expr);
	SgExprStatement* dvm_seen_call_stmt = buildFunctionCallStmt(SgName("dvm_seen"), buildVoidType(), fun_param_list, pragma->get_scope());
	insertStatement(pragma, dvm_seen_call_stmt);

	fun_param_list = buildExprListExp();
	fun_param_list->append_expression(var);
	SgExprStatement* dvm_end_call_stmt = buildFunctionCallStmt(SgName("dvm_end"), buildVoidType(), fun_param_list, pragma->get_scope());
	insertStatement(pragma, dvm_end_call_stmt);
	clog << "inserted function calls" << endl;
	removeStatement(pragma);
	clog << "removed pragma" << endl;
}

bool PragmaHandler::afs_match_undefined_identifier(string &id)
{
	//bool result = false;
	char buffer[OFS_MAX_LEN];
	const char* old_char = c_char;
	afs_skip_whitespace();
	// check for the first char
	// Must be either of letter or _ (extended to support this)
	if (!(afs_is_letter() || (*c_char=='_'))) {
		c_char = old_char;
		return false;
	}
	// We have a legal identifier now
	int i = 0;
	do {
		buffer[i] = *c_char;
		i++;
		c_char++;
	} while (afs_is_identifier_char());
	
	buffer[i]= '\0';
	id = buffer;
	return true;
}

void PragmaHandler::recognize_dvm_1(SgPragmaDeclaration *pragma_decl)
{
	clog << "dvm <id>(<exp>) pragma found" << endl;
	string id;
	if (!afs_match_undefined_identifier(id))
		throw string("incorrect pragma format");
	SgVarRefExp *var = buildVarRefExp(id);
	if (!afs_match_char('('))
		throw string("incorrect pragma format");
	if (!afs_match_expression())
		throw string("incorrect pragma format");
	SgExpression *expr = isSgExpression(deepCopy(c_parsed_node));
	if (!expr)
		throw string ();
	if (!afs_match_char(')'))
		throw string("incorrect pragma format");
	
	handle_dvm_1(pragma_decl, var, expr);
}

void PragmaHandler::recognize_dvm_2(SgPragmaDeclaration *pragma_decl)
{
	clog << "dvm template <id>[<exp>] pragma found" << endl;
	if (!afs_match_identifier())
		throw string("incorrect pragma format");
	SgVarRefExp *var = isSgVarRefExp(deepCopy(c_parsed_node));
	if (!var)
		throw string("not variable, but an instance of ") + c_parsed_node->sage_class_name() + string(" before the brackets");
	if (!afs_match_char('['))
		throw string("incorrect pragma format");
	if (!afs_match_expression())
		throw string("incorrect pragma format");
	SgExpression *expr = isSgExpression(deepCopy(c_parsed_node));
	if (!expr)
		throw string ();
	if (!afs_match_char(']'))
		throw string("incorrect pragma format");
	
	handle_dvm_2(pragma_decl, var, expr);
}

void PragmaHandler::handle_pragma(SgPragmaDeclaration *pragma_decl)
{
	string pragma(pragma_decl->get_pragma()->get_pragma()); 
	c_char = pragma.c_str();
	c_sgnode = pragma_decl;
		assert(c_sgnode != NULL);
	if (!afs_match_substr("dvm"))
		return;
	if (afs_match_substr("template"))
		recognize_dvm_2(pragma_decl);
	else
		recognize_dvm_1(pragma_decl);
}

int main(int argc, char * argv[])
{
	SgProject* project = frontend(argc,argv);

	VisitorTraversal global_traversal;
	global_traversal.traverseInputFiles(project, preorder);

	vector<SgPragmaDeclaration *> pragmas = global_traversal.get_pragmas();
	if (!pragmas.size())
		return 0;
	PragmaHandler handler;
	for (unsigned i = 0; i < pragmas.size(); i++) {
		SgPragmaDeclaration* pragma = pragmas[i];
		try {
			handler.handle_pragma(pragma);
		} catch (string s) {
			clog << "Error handling pragma: " << (s.size() ? s : "unknown error") << endl;
		}
	}

	UnparseFormatHelp *f = new UnparseFormatCustom();
	project->unparse(f);
	return 0; 
}

